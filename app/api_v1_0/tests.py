# ~ from django.test import TestCase
import json

import pytest
from rest_framework.test import APIClient, RequestsClient


API_PREFIX = '/api_v1_0'
CONTENT_TYPE = 'application/json'
EXISTENT_EMAIL = 'Karley_Dach@jasper.info'
INEXISTENT_EMAIL = 'inexistent_user@inexistent.domain'
INVALID_EMAIL = 'invalid email'


class TestUserPosts:

    def test_get_user_posts_by_email_happy_path(self, client):
        data = json.dumps({'email': EXISTENT_EMAIL})
        response = client.generic('GET', f'{API_PREFIX}/user_posts/', data=data, content_type=CONTENT_TYPE)
        assert response.status_code == 200
        response_json = response.json()

        assert 'user' in response_json
        resp_user = response_json['user']
        assert 'email' in resp_user
        resp_email = resp_user['email']
        assert resp_email == EXISTENT_EMAIL
        assert all([key in resp_user for key in ['id', 'name', 'phone', 'username', 'website']])

        assert 'posts' in response_json
        resp_posts = response_json['posts']
        if resp_posts:
            assert all([key in resp_posts[0] for key in ['body', 'id', 'title']])
            assert 'userId' not in resp_posts[0]

    def test_get_user_posts_by_email_no_email(self, client):
        data = json.dumps({})
        response = client.generic('GET', f'{API_PREFIX}/user_posts/', data=data, content_type=CONTENT_TYPE)
        assert response.status_code == 400
        response_json = response.json()
        assert 'detail' in response_json
        assert 'Email parameter is required.' in response_json['detail']

    def test_get_user_posts_by_email_inexistent_email(self, client):
        data = json.dumps({'email': INEXISTENT_EMAIL})
        response = client.generic('GET', f'{API_PREFIX}/user_posts/', data=data, content_type=CONTENT_TYPE)
        assert response.status_code == 404
        response_json = response.json()
        assert 'detail' in response_json
        assert 'No user found by email:' in response_json['detail']

    def test_get_user_posts_by_email_invalid_email(self, client):
        data = json.dumps({'email': INVALID_EMAIL})
        response = client.generic('GET', f'{API_PREFIX}/user_posts/', data=data, content_type=CONTENT_TYPE)
        assert response.status_code == 400
        response_json = response.json()
        assert 'detail' in response_json
        assert 'Invalid email format:' in response_json['detail']

