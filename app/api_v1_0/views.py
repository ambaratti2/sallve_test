import json
from json.decoder import JSONDecodeError

import requests

import api_v1_0.serializers as serial
from main.settings import OPEN_API_URL
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.views import APIView


class SallveAPIException(APIException):
    def __init__(self, detail='', status_code=400, default_code='Bad Request'):
        self.detail = detail
        self.status_code = status_code
        self.default_code = default_code


class UserPosts(APIView):
    """ User posts service. """

    def get(self, request):
        """ GET a list of user posts.

            input:
                body:
                    email: user email from whom we'll retrieve the posts.
        """

        self.email = request.data.get('email')
        self.check_email()

        user_data = self.retrieve_user_data()

        serialized_user = serial.UserSerializer(data=user_data)
        if not serialized_user.is_valid():
            raise SallveAPIException(
                f'Validation error for user data: {serialized_user.errors}'
            )

        self.user_id = user_data.get('id')

        user_posts = self.retieve_user_posts()

        serialized_posts = serial.PostsSerializer(data=user_posts, many=True)
        if not serialized_posts.is_valid():
            raise SallveAPIException(
                f'Validation errors for user posts: {serialized_posts.errors}'
            )

        serialized_user_posts = serial.UserPostsSerializer(
            {'user': serialized_user.data, 'posts': serialized_posts.data}
        )

        return Response(serialized_user_posts.data)

    def check_email(self):
        """ Check email format using a serializer as validator. """

        if not self.email:
            raise SallveAPIException('Email parameter is required.')
        serialized_email = serial.EmailSerializer(data={'email': self.email})
        if not serialized_email.is_valid():
            raise SallveAPIException(f'Invalid email format: {self.email}.')

    def retrieve_user_data(self):
        response = requests.get(f'{OPEN_API_URL}/users', params={'email': self.email})
        if response.status_code != 200:
            raise SallveAPIException(
                'Could not retrieve user data.', status_code=response.status_code
            )
        try:
            user_response = json.loads(response.content)
        except JSONDecodeError:
            raise SallveAPIException('Invalid content for user data')

        if not user_response:
            raise SallveAPIException(
                f'No user found by email: {self.email}', status_code=404
            )
        if len(user_response) > 1:
            raise SallveAPIException(f'Found more than one user for email: {self.email}')
        return user_response[0]

    def retieve_user_posts(self):
        response = requests.get(
            f'{OPEN_API_URL}/posts', params={'userId': self.user_id}
        )
        if response.status_code != 200:
            raise SallveAPIException(
                'Could not retrieve user posts.', status_code=response.status_code
            )
        try:
            posts_response = json.loads(response.content)
        except JSONDecodeError:
            raise SallveAPIException('Invalid content for user posts.')
        return posts_response
