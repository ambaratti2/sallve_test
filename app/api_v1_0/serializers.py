from rest_framework import serializers


class CoordinatesSerializer(serializers.Serializer):
    """ Coordinates serializer for JSONPlaceholder API. """
    lat = serializers.CharField(required=False)
    lng = serializers.CharField(required=False)


class AddressSerializer(serializers.Serializer):
    """ Address serializer for JSONPlaceholder API. """
    city = serializers.CharField(required=False)
    geo = CoordinatesSerializer(required=False)
    street = serializers.CharField(required=False)
    suite = serializers.CharField(required=False)
    zipcode = serializers.CharField(required=False)


class CompanySerializer(serializers.Serializer):
    """ Company serializer for JSONPlaceholder API. """
    bs = serializers.CharField(required=False)
    catchPhrase = serializers.CharField(required=False)
    name = serializers.CharField(required=False)


class UserSerializer(serializers.Serializer):
    """ User serializer for JSONPlaceholder API. """
    id = serializers.IntegerField()
    name = serializers.CharField(required=False)
    phone = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)
    username = serializers.CharField(required=False)
    website = serializers.CharField(required=False)
    company = CompanySerializer(required=False)
    address = AddressSerializer(required=False)


class PostsSerializer(serializers.Serializer):
    """ Posts serializer for JSONPlaceholder API. """
    id = serializers.IntegerField()
    title = serializers.CharField(required=False)
    body = serializers.CharField(required=False)
    userId = serializers.IntegerField(read_only=True)


class UserPostsSerializer(serializers.Serializer):
    """ User posts output serializer. """
    user = UserSerializer()
    posts = PostsSerializer(many=True)


class EmailSerializer(serializers.Serializer):
    """ Serializer to check email format. """
    email = serializers.EmailField()
