# Sallve

## Backend Test

### Sumary:

This is a test API for retrieval of posts of users by their email on an open API.

### Test premisses:

There are some conditions that exists because this is only a test and to don't extend it too much:

- A real web server was not configured to this test. The simple one offered by Django is to be used by it.
- A separate branch was not used but the default master branch.
- The DEBUG setting was keep activated.
- There is no docker or integration with a production HTTP server.
- The project uses HTTP (instead of HTTPS).
- No mocking was created for the tests, because we're using only GETs on an open test API.



### System Configuration:

Below are the steps to configure an environment to run the project:

- Clone this project:

    ```bash
    git clone https://ambaratti2@bitbucket.org/ambaratti2/sallve_test.git
    ```

- Go into the project directory:

    ```bash
    cd sallve_test
    ```

- Make sure you have Python 3.7+ installed in your machine (I've not tested with Python 3.6, but it shoukd work).
- Make sure you have pip installed too (https://pip.pypa.io/en/stable/installing/).
- Install and create a virtual environment for Python (https://docs.python.org/3/tutorial/venv.html).
- On the virtual environment install the required packages:

    ```bash
    pip install -r requirements.txt
    ```

- Go to application directory:

    ```bash
    cd app
    ```

- Run the local server:

    ```bash
    ./manage.py runserver
    ```

  This script takes into account that Python 3 is accessed through `python` command.
  If your distro uses `python3` for that, run the server like this:

    ```bash
      python3 manage.py runserver
    ```

### API documentation:

#### List of all posts of a user:

- **Url**: http://localhost:8000/api_v1_0/user_posts/
- **Method**: GET
- **Input parameters**:
    - **name**: email
    - **description**: User email from whom we want to list all posts.
    - **parameter type**: data body parameter (application/json)
    - **data type**: string
    - **required**: true

- **Output layout**:
    ```json
    {
        "posts": [
            {
                "body": "sunt dolores aut doloribus\ndolore doloribus voluptates tempora et\ndoloremque et quo\ncum asperiores sit consectetur dolorem",
                "id": 51,
                "title": "soluta aliquam aperiam consequatur illo quis voluptas"
            },
            {
                "body": "iusto est quibusdam fuga quas quaerat molestias\na enim ut sit accusamus enim\ntemporibus iusto accusantium provident architecto\nsoluta esse reprehenderit qui laborum",
                "id": 52,
                "title": "qui enim et consequuntur quia animi quis voluptate quibusdam"
            },

            ...

            {
                "body": "asperiores sunt ab assumenda cumque modi velit\nqui esse omnis\nvoluptate et fuga perferendis voluptas\nillo ratione amet aut et omnis",
                "id": 60,
                "title": "consequatur placeat omnis quisquam quia reprehenderit fugit veritatis facere"
            }
        ],
        "user": {
            "address": {
                "city": "South Christy",
                "geo": {
                    "lat": "-71.4197",
                    "lng": "71.7478"
                },
                "street": "Norberto Crossing",
                "suite": "Apt. 950",
                "zipcode": "23505-1337"
            },
            "company": {
                "bs": "e-enable innovative applications",
                "catchPhrase": "Synchronised bottom-line interface",
                "name": "Considine-Lockman"
            },
            "email": "Karley_Dach@jasper.info",
            "id": 6,
            "name": "Mrs. Dennis Schulist",
            "phone": "1-477-935-8478 x6430",
            "username": "Leopoldo_Corkery",
            "website": "ola.org"
        }
    }
    ```
- **Output status**:    200

- **Error conditions**:

    - **No email parameter received**:
    - Output status:    400     (Bad Request)
    - Message:          "Email parameter is required."

    - **User email not found**:
    - Output status:    404     (Not Found)
    - Message:          "No user found by email: <email>."

    - **More than one user found for provided email**:
    - Output status:    404     (Not Found)
    - Message:          "Found more than one user for email:  <email>."

    - **Invalid email format**:
    - Output status:    400     (Bad Request)
    - Message:          "Invalid email format: <email>."

    - **Bad format from JSONPlaceholder API**:
    - Output status:    400     (Bad Request)
    - Message:          "Validation error for {user | posts} data: "

    - **Invalid data format from JSONPlaceholder API**:
    - Output status:    400     (Bad Request)
    - Message:          "Invalid content for user posts."


### Functional tests:

The tests can be run on virtual environment, in sallve_test/app directory running:
```bash
pytest
```

